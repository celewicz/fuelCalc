import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../refuels.dart';

class RefuelTile extends StatelessWidget {
  final String currency;
  final String total;
  final String price;
  final String amount;
  final String id;
  final String date;
  RefuelTile(
      {this.total,
      this.price,
      this.amount,
      this.currency = 'PLN',
      this.id,
      this.date});
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text('$total $currency, ($price x $amount)'),
        subtitle: Text('Lublin/Rury, Date: $date'),
        trailing: SizedBox(
          width: 60,
          child: Icon(Icons.more_vert),
        ),
        dense: true,
        onLongPress: () {
          Provider.of<Refuels>(context).selectRefuel(id);
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_fuel/widgets/RefuelTile.dart';
import 'package:flutter_fuel/refuels.dart';
import 'package:provider/provider.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class RefuelList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<Refuels>(builder: (context, refuelsData, child) {
      return ListView.builder(
        itemBuilder: (context, index) {
          var refuel = refuelsData.refuels[index];
          return Slidable(
            actionPane: SlidableDrawerActionPane(),
            actionExtentRatio: 0.25,
            key: Key(refuel.id.toString()),
            child: RefuelTile(
              total: refuel.spend.toStringAsFixed(2),
              price: refuel.price.toString(),
              amount: refuel.amount.toString(),
              id: refuel.id.toString(),
              date: refuel.date.toString(),
            ),
            secondaryActions: <Widget>[
              IconSlideAction(
                  caption: 'More',
                  color: Colors.green,
                  icon: Icons.edit,
                  onTap: () {
                    refuelsData.selectRefuel(refuel.id);
                    print('select on slide');
                  }),
              IconSlideAction(
                caption: 'Delete',
                color: Colors.red,
                icon: Icons.delete,
                onTap: () {
                  refuelsData.removeRefuel(refuel);
                  refuelsData.refuels.removeAt(index);
                },
              ),
            ],
          );
        },
        itemCount: refuelsData.refuels.length,
      );
    });
  }
}

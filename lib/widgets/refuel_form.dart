import 'package:flutter/material.dart';
import 'package:flutter_fuel/model/Refuel.dart';
import 'package:flutter_fuel/refuels.dart';
import 'package:flutter_fuel/constants.dart';
import 'package:provider/provider.dart';

class RefuelForm extends StatefulWidget {
  @override
  _RefuelFormState createState() => _RefuelFormState();
}

class _RefuelFormState extends State<RefuelForm> {
  var priceController = TextEditingController();
  var amountController = TextEditingController();
  var mileageController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    priceController.dispose();
    amountController.dispose();
    mileageController.dispose();
    super.dispose();
  }

  var price;
  var amount;
  var mileage;

  clearFormData() {
    priceController.clear();
    amountController.clear();
    mileageController.clear();
    price = null;
    amount = null;
    mileage = null;
  }

  @override
  Widget build(BuildContext context) {
    var newRefuel = Provider.of<Refuels>(context).newRefuel;
    var _editedRefuel = Provider.of<Refuels>(context).selectedRefuel;

    if (newRefuel == null || newRefuel.price == null) {
      clearFormData();
    } else {
      price = newRefuel.price;
      priceController.text = newRefuel.price.toString();
    }

    if (newRefuel == null || newRefuel.amount == null) {
      clearFormData();
    } else {
      amount = newRefuel.amount;
      amountController.text = newRefuel.amount.toString();
    }

    if (newRefuel == null || newRefuel.mileage == null) {
      clearFormData();
    } else {
      mileage = newRefuel.mileage;
      mileageController.text = newRefuel.mileage.toString();
    }

    if (_editedRefuel != null) {
      print('editing refuel ----- ');
      print(_editedRefuel.mileage);
      price = _editedRefuel.price;
      amount = _editedRefuel.amount;
      mileage = _editedRefuel.mileage;
      priceController.text = _editedRefuel.price.toString();
      amountController.text = _editedRefuel.amount.toString();
      mileageController.text = _editedRefuel.mileage.toString();
    }

    void editSaveRefuel(Refuel refuel) {
      if (_editedRefuel != null) {
        Provider.of<Refuels>(context).saveRefuel(_editedRefuel.id, refuel);
      } else {
        Provider.of<Refuels>(context).addRefuel(refuel);
      }
    }

    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              SizedBox(
                width: 200.0,
                child: TextField(
                  controller: priceController,
                  decoration: InputDecoration(labelText: 'Fuel price'),
                  inputFormatters: [fuelPriceFormatter],
                  keyboardType: TextInputType.number,
                  onChanged: (newPrice) {
                    price = double.parse(newPrice);
                  },
                ),
              ),
              SizedBox(
                width: 20.0,
              ),
              Expanded(
                  child: TextField(
                controller: amountController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(labelText: 'Fuel amount'),
                onChanged: (newAmount) {
                  amount = double.parse(newAmount);
                },
              )),
            ],
          ),
          Row(
            children: <Widget>[
              SizedBox(
                width: 200,
                child: TextField(
                  controller: mileageController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Current mileage'),
                  onChanged: (newMileage) {
                    mileage = double.parse(newMileage);
                  },
                ),
              ),
              SizedBox(
                width: 20.0,
              ),
              Expanded(
                  child: RaisedButton(
                child: Icon(Icons.add),
                onPressed: () {
                  editSaveRefuel(Refuel(
                    price: price,
                    amount: amount,
                    mileage: mileage,
                  ));
                  clearFormData();
                },
              )),
            ],
          )
        ],
      ),
    );
  }
}
